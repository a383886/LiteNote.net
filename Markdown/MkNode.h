#pragma once

#include <vector>
#include <string>
#include <memory>

class AnchorNode;
typedef std::shared_ptr<AnchorNode> AnchorNodePtr;

class AnchorNode
{
public:
	AnchorNode();
    void AppendCh(AnchorNode* node);

public:
    int m_level;
    std::string m_anchorID;
    std::string m_name;
    std::vector<AnchorNode*> m_children;
};

std::pair<std::string, AnchorNode*> SyntaxMk(std::vector<std::string> &lines, const std::string &file_dir, int font_size);

//void ReleaseAnchorNode(AnchorNode *node);

/*
* @file_dir 文件所在的路径
* @out_str 返回字符串，调用者负责声明足够大的空间
*	返回xml结构, 其root下一个节点为内容，第二个节点为锚点
*/
extern "C" __declspec(dllexport) void SynatxMarkdown(const char *text, const char *file_dir, int font_size, char *out_str);
