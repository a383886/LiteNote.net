﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteNote
{
    class AnchorNode
    {
        public int Level { get; set; }
        public string AnchorID { get; set; }
        public string Name { get; set; }
        public List<AnchorNode> Children { get; set; }

        public AnchorNode()
        {
            Children = new List<AnchorNode>();
        }
    }
}
