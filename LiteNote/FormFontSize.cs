﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LiteNote
{
    public partial class FormFontSize : Form
    {
        public FormFontSize()
        {
            InitializeComponent();
        }

        public int FontSize { get; set; }

        public void SetNumberValue(int value)
        {
            numericUpDown1.Value = value;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            //this.DialogResult
            DialogResult = DialogResult.OK;
            FontSize = (int)numericUpDown1.Value;
            Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
