﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LiteNote
{
    partial class FormAddWorkset : Form
    {
        public FormAddWorkset()
        {
            InitializeComponent();
        }

        private void FormAddWorkset_Load(object sender, EventArgs e)
        {

        }

        // path, name
        public Tuple<string, string> GetWorksetInfo()
        {
            return new Tuple<string, string>(txtPath.Text, txtName.Text);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnOpenFold_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK) {
                string path = folderBrowserDialog1.SelectedPath;
                /*string name = Path.GetFileName(path);
                settings.WorksetNames.Add(name);
                settings.WorksetPaths.Add(path);
                settings.Save();

                AddRootNode(name, folderBrowserDialog1.SelectedPath);
                */
                txtPath.Text = path;

                var s = Path.GetFileName(path);
                if (s.Length == 0)
                    s = path;

                txtName.Text = s;
            }
        }

      
    }
}
