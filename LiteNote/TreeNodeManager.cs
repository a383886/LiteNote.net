﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace LiteNote
{
    class TreeNodeManager
    {

        public TreeNodeManager()
        {

        }

        public static bool IsAddChildNode(TreeNode node)
        {
            foreach (TreeNode n in node.Nodes) {
                if (n.Nodes.Count != 0)
                    return false;
            }
            return true;
        }

    }//end class
}
