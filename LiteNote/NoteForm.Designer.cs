﻿namespace LiteNote
{
    partial class NoteForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NoteForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.treeView2 = new System.Windows.Forms.TreeView();
            this.contextMenuBlack = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextItemReload = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.contextItemAddWorkset = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuFold = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextItemAddNote = new System.Windows.Forms.ToolStripMenuItem();
            this.contextItemAddFold = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripSeparator();
            this.contextItemRename2 = new System.Windows.Forms.ToolStripMenuItem();
            this.contextItemDelete2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.contextItemExplorer2 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.item文件 = new System.Windows.Forms.ToolStripMenuItem();
            this.itemLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.itemAddWorkset = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.itemExit = new System.Windows.Forms.ToolStripMenuItem();
            this.item编辑 = new System.Windows.Forms.ToolStripMenuItem();
            this.itemInsertImg = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripSeparator();
            this.itemEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.itemRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.itemFind = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.itemAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.itemAddFold = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.itemRename = new System.Windows.Forms.ToolStripMenuItem();
            this.itemDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.item设置 = new System.Windows.Forms.ToolStripMenuItem();
            this.itemFontSize = new System.Windows.Forms.ToolStripMenuItem();
            this.itemEditApp = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripSeparator();
            this.itemIsCollapseBrother = new System.Windows.Forms.ToolStripMenuItem();
            this.itemOneClickMode = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripSeparator();
            this.itemCharset = new System.Windows.Forms.ToolStripMenuItem();
            this.item帮助 = new System.Windows.Forms.ToolStripMenuItem();
            this.itemHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.itemAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuNode = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextItemEdit2 = new System.Windows.Forms.ToolStripMenuItem();
            this.contextItemRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.contextItemRename = new System.Windows.Forms.ToolStripMenuItem();
            this.contextItemDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.contextItemExplorer = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusLabelFold = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusLabelNode = new System.Windows.Forms.ToolStripStatusLabel();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.openFileDialogEditApp = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.contextMenuWorkset = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextItemWorksetAddNote = new System.Windows.Forms.ToolStripMenuItem();
            this.contextItemWorksetAddFold = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripSeparator();
            this.contextItemWorksetRename = new System.Windows.Forms.ToolStripMenuItem();
            this.contextItemWorksetRemove = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripSeparator();
            this.contextItemWorksetExplorer = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialogInsertImg = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.contextMenuBlack.SuspendLayout();
            this.contextMenuFold.SuspendLayout();
            this.menuMain.SuspendLayout();
            this.contextMenuNode.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.contextMenuWorkset.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 35);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeView1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1227, 698);
            this.splitContainer1.SplitterDistance = 236;
            this.splitContainer1.SplitterWidth = 6;
            this.splitContainer1.TabIndex = 1;
            this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeView1.FullRowSelect = true;
            this.treeView1.HideSelection = false;
            this.treeView1.HotTracking = true;
            this.treeView1.ImageIndex = 0;
            this.treeView1.ImageList = this.imageList1;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Name = "treeView1";
            this.treeView1.SelectedImageIndex = 0;
            this.treeView1.Size = new System.Drawing.Size(232, 694);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.treeView1_AfterLabelEdit);
            this.treeView1.AfterCollapse += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterCollapse);
            this.treeView1.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterExpand);
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            this.treeView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.treeView1_KeyDown);
            this.treeView1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treeView1_KeyUp);
            this.treeView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.treeView1_MouseDown);
            this.treeView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.treeView1_MouseUp);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "workset.png");
            this.imageList1.Images.SetKeyName(1, "fold.png");
            this.imageList1.Images.SetKeyName(2, "note.png");
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.webBrowser1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.treeView2);
            this.splitContainer2.Size = new System.Drawing.Size(985, 698);
            this.splitContainer2.SplitterDistance = 585;
            this.splitContainer2.SplitterWidth = 6;
            this.splitContainer2.TabIndex = 3;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(22, 22);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(581, 694);
            this.webBrowser1.TabIndex = 3;
            this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
            this.webBrowser1.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.webBrowser1_PreviewKeyDown);
            // 
            // treeView2
            // 
            this.treeView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView2.HideSelection = false;
            this.treeView2.Location = new System.Drawing.Point(0, 0);
            this.treeView2.Name = "treeView2";
            this.treeView2.Size = new System.Drawing.Size(390, 694);
            this.treeView2.TabIndex = 0;
            this.treeView2.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView2_AfterSelect);
            // 
            // contextMenuBlack
            // 
            this.contextMenuBlack.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuBlack.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contextItemReload,
            this.toolStripMenuItem6,
            this.contextItemAddWorkset});
            this.contextMenuBlack.Name = "contextMenuTree";
            this.contextMenuBlack.Size = new System.Drawing.Size(179, 70);
            // 
            // contextItemReload
            // 
            this.contextItemReload.Name = "contextItemReload";
            this.contextItemReload.Size = new System.Drawing.Size(178, 30);
            this.contextItemReload.Text = "重载";
            this.contextItemReload.Click += new System.EventHandler(this.ItemReLoad_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(175, 6);
            // 
            // contextItemAddWorkset
            // 
            this.contextItemAddWorkset.Image = global::LiteNote.Properties.Resources.workset;
            this.contextItemAddWorkset.Name = "contextItemAddWorkset";
            this.contextItemAddWorkset.Size = new System.Drawing.Size(178, 30);
            this.contextItemAddWorkset.Text = "添加工作集";
            this.contextItemAddWorkset.Click += new System.EventHandler(this.itemAddWorkset_Click);
            // 
            // contextMenuFold
            // 
            this.contextMenuFold.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuFold.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contextItemAddNote,
            this.contextItemAddFold,
            this.toolStripMenuItem12,
            this.contextItemRename2,
            this.contextItemDelete2,
            this.toolStripMenuItem3,
            this.contextItemExplorer2});
            this.contextMenuFold.Name = "contextMenuStripTree";
            this.contextMenuFold.Size = new System.Drawing.Size(232, 166);
            // 
            // contextItemAddNote
            // 
            this.contextItemAddNote.Image = global::LiteNote.Properties.Resources.note;
            this.contextItemAddNote.Name = "contextItemAddNote";
            this.contextItemAddNote.Size = new System.Drawing.Size(231, 30);
            this.contextItemAddNote.Text = "新建笔记";
            this.contextItemAddNote.Click += new System.EventHandler(this.ItemAddNote_Click);
            // 
            // contextItemAddFold
            // 
            this.contextItemAddFold.Image = global::LiteNote.Properties.Resources.fold;
            this.contextItemAddFold.Name = "contextItemAddFold";
            this.contextItemAddFold.Size = new System.Drawing.Size(231, 30);
            this.contextItemAddFold.Text = "新建文件夹";
            this.contextItemAddFold.Click += new System.EventHandler(this.ItemAddFold_Click);
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(228, 6);
            // 
            // contextItemRename2
            // 
            this.contextItemRename2.Image = global::LiteNote.Properties.Resources.rename;
            this.contextItemRename2.Name = "contextItemRename2";
            this.contextItemRename2.Size = new System.Drawing.Size(231, 30);
            this.contextItemRename2.Text = "重命名";
            this.contextItemRename2.Click += new System.EventHandler(this.ItemRename_Click);
            // 
            // contextItemDelete2
            // 
            this.contextItemDelete2.Image = global::LiteNote.Properties.Resources.delete;
            this.contextItemDelete2.Name = "contextItemDelete2";
            this.contextItemDelete2.Size = new System.Drawing.Size(231, 30);
            this.contextItemDelete2.Text = "删除";
            this.contextItemDelete2.Click += new System.EventHandler(this.ItemDelete_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(228, 6);
            // 
            // contextItemExplorer2
            // 
            this.contextItemExplorer2.Image = global::LiteNote.Properties.Resources.explorer;
            this.contextItemExplorer2.Name = "contextItemExplorer2";
            this.contextItemExplorer2.Size = new System.Drawing.Size(231, 30);
            this.contextItemExplorer2.Text = "在Explorer中打开";
            this.contextItemExplorer2.Click += new System.EventHandler(this.ItemExplorer_Click);
            // 
            // menuMain
            // 
            this.menuMain.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(150)), true);
            this.menuMain.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.item文件,
            this.item编辑,
            this.item设置,
            this.item帮助});
            this.menuMain.Location = new System.Drawing.Point(0, 0);
            this.menuMain.Name = "menuMain";
            this.menuMain.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuMain.Size = new System.Drawing.Size(1227, 32);
            this.menuMain.TabIndex = 2;
            this.menuMain.Text = "menuStrip1";
            // 
            // item文件
            // 
            this.item文件.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemLoad,
            this.itemAddWorkset,
            this.toolStripSeparator,
            this.itemExit});
            this.item文件.Name = "item文件";
            this.item文件.Size = new System.Drawing.Size(80, 28);
            this.item文件.Text = "文件(&F)";
            // 
            // itemLoad
            // 
            this.itemLoad.Name = "itemLoad";
            this.itemLoad.Size = new System.Drawing.Size(186, 34);
            this.itemLoad.Text = "重载";
            this.itemLoad.Click += new System.EventHandler(this.ItemReLoad_Click);
            // 
            // itemAddWorkset
            // 
            this.itemAddWorkset.Image = global::LiteNote.Properties.Resources.workset;
            this.itemAddWorkset.Name = "itemAddWorkset";
            this.itemAddWorkset.Size = new System.Drawing.Size(186, 34);
            this.itemAddWorkset.Text = "添加工作集";
            this.itemAddWorkset.Click += new System.EventHandler(this.itemAddWorkset_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(183, 6);
            // 
            // itemExit
            // 
            this.itemExit.Name = "itemExit";
            this.itemExit.Size = new System.Drawing.Size(186, 34);
            this.itemExit.Text = "退出(&X)";
            this.itemExit.Click += new System.EventHandler(this.ItemExit_Click);
            // 
            // item编辑
            // 
            this.item编辑.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemInsertImg,
            this.toolStripMenuItem11,
            this.itemEdit,
            this.itemRefresh,
            this.itemFind,
            this.toolStripMenuItem2,
            this.itemAdd,
            this.itemAddFold,
            this.toolStripMenuItem5,
            this.itemRename,
            this.itemDelete});
            this.item编辑.Name = "item编辑";
            this.item编辑.Size = new System.Drawing.Size(80, 28);
            this.item编辑.Text = "编辑(&E)";
            this.item编辑.Click += new System.EventHandler(this.item编辑_Click);
            // 
            // itemInsertImg
            // 
            this.itemInsertImg.Name = "itemInsertImg";
            this.itemInsertImg.Size = new System.Drawing.Size(186, 34);
            this.itemInsertImg.Text = "插入图片";
            this.itemInsertImg.Click += new System.EventHandler(this.itemInsertImg_Click);
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(183, 6);
            // 
            // itemEdit
            // 
            this.itemEdit.Image = global::LiteNote.Properties.Resources.edit;
            this.itemEdit.Name = "itemEdit";
            this.itemEdit.ShortcutKeyDisplayString = "F4";
            this.itemEdit.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.itemEdit.Size = new System.Drawing.Size(186, 34);
            this.itemEdit.Text = "编辑";
            this.itemEdit.Click += new System.EventHandler(this.ItemEdit_Click);
            // 
            // itemRefresh
            // 
            this.itemRefresh.Image = global::LiteNote.Properties.Resources.refresh;
            this.itemRefresh.Name = "itemRefresh";
            this.itemRefresh.ShortcutKeyDisplayString = "F5";
            this.itemRefresh.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.itemRefresh.Size = new System.Drawing.Size(186, 34);
            this.itemRefresh.Text = "刷新";
            this.itemRefresh.Click += new System.EventHandler(this.ItemRefresh_Click);
            // 
            // itemFind
            // 
            this.itemFind.Name = "itemFind";
            this.itemFind.Size = new System.Drawing.Size(186, 34);
            this.itemFind.Text = "查找";
            this.itemFind.Click += new System.EventHandler(this.itemFind_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(183, 6);
            // 
            // itemAdd
            // 
            this.itemAdd.Image = global::LiteNote.Properties.Resources.note;
            this.itemAdd.Name = "itemAdd";
            this.itemAdd.Size = new System.Drawing.Size(186, 34);
            this.itemAdd.Text = "新建笔记";
            this.itemAdd.Click += new System.EventHandler(this.ItemAddNote_Click);
            // 
            // itemAddFold
            // 
            this.itemAddFold.Image = global::LiteNote.Properties.Resources.fold;
            this.itemAddFold.Name = "itemAddFold";
            this.itemAddFold.Size = new System.Drawing.Size(186, 34);
            this.itemAddFold.Text = "新建文件夹";
            this.itemAddFold.Click += new System.EventHandler(this.ItemAddFold_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(183, 6);
            // 
            // itemRename
            // 
            this.itemRename.Image = global::LiteNote.Properties.Resources.rename;
            this.itemRename.Name = "itemRename";
            this.itemRename.ShortcutKeyDisplayString = "F2";
            this.itemRename.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.itemRename.Size = new System.Drawing.Size(186, 34);
            this.itemRename.Text = "重命名";
            this.itemRename.Click += new System.EventHandler(this.ItemRename_Click);
            // 
            // itemDelete
            // 
            this.itemDelete.Image = global::LiteNote.Properties.Resources.delete;
            this.itemDelete.Name = "itemDelete";
            this.itemDelete.Size = new System.Drawing.Size(186, 34);
            this.itemDelete.Text = "删除";
            this.itemDelete.Click += new System.EventHandler(this.ItemDelete_Click);
            // 
            // item设置
            // 
            this.item设置.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemFontSize,
            this.itemEditApp,
            this.toolStripMenuItem7,
            this.itemIsCollapseBrother,
            this.itemOneClickMode,
            this.toolStripMenuItem10,
            this.itemCharset});
            this.item设置.Name = "item设置";
            this.item设置.Size = new System.Drawing.Size(80, 28);
            this.item设置.Text = "设置(&S)";
            // 
            // itemFontSize
            // 
            this.itemFontSize.Image = global::LiteNote.Properties.Resources.size;
            this.itemFontSize.Name = "itemFontSize";
            this.itemFontSize.Size = new System.Drawing.Size(254, 30);
            this.itemFontSize.Text = "字号大小";
            this.itemFontSize.Click += new System.EventHandler(this.itemFontSize_Click);
            // 
            // itemEditApp
            // 
            this.itemEditApp.Name = "itemEditApp";
            this.itemEditApp.Size = new System.Drawing.Size(254, 30);
            this.itemEditApp.Text = "外部编辑程序";
            this.itemEditApp.Click += new System.EventHandler(this.itemEditApp_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(251, 6);
            // 
            // itemIsCollapseBrother
            // 
            this.itemIsCollapseBrother.Checked = true;
            this.itemIsCollapseBrother.CheckState = System.Windows.Forms.CheckState.Checked;
            this.itemIsCollapseBrother.Name = "itemIsCollapseBrother";
            this.itemIsCollapseBrother.Size = new System.Drawing.Size(254, 30);
            this.itemIsCollapseBrother.Text = "展开时收起兄弟节点";
            this.itemIsCollapseBrother.Click += new System.EventHandler(this.itemIsCollapseBrother_Click);
            // 
            // itemOneClickMode
            // 
            this.itemOneClickMode.Checked = true;
            this.itemOneClickMode.CheckState = System.Windows.Forms.CheckState.Checked;
            this.itemOneClickMode.Name = "itemOneClickMode";
            this.itemOneClickMode.Size = new System.Drawing.Size(254, 30);
            this.itemOneClickMode.Text = "树节点单击模式";
            this.itemOneClickMode.Click += new System.EventHandler(this.itemOneClickMode_Click);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(251, 6);
            // 
            // itemCharset
            // 
            this.itemCharset.Name = "itemCharset";
            this.itemCharset.Size = new System.Drawing.Size(254, 30);
            this.itemCharset.Text = "编码";
            this.itemCharset.Click += new System.EventHandler(this.itemCharset_Click);
            // 
            // item帮助
            // 
            this.item帮助.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemHelp,
            this.toolStripSeparator8,
            this.itemAbout});
            this.item帮助.Name = "item帮助";
            this.item帮助.Size = new System.Drawing.Size(84, 28);
            this.item帮助.Text = "帮助(&H)";
            // 
            // itemHelp
            // 
            this.itemHelp.Name = "itemHelp";
            this.itemHelp.Size = new System.Drawing.Size(165, 30);
            this.itemHelp.Text = "帮助(&H)";
            this.itemHelp.Click += new System.EventHandler(this.ItemHelp_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(162, 6);
            // 
            // itemAbout
            // 
            this.itemAbout.Name = "itemAbout";
            this.itemAbout.Size = new System.Drawing.Size(165, 30);
            this.itemAbout.Text = "关于(&A)...";
            this.itemAbout.Click += new System.EventHandler(this.ItemAbout_Click);
            // 
            // contextMenuNode
            // 
            this.contextMenuNode.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuNode.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contextItemEdit2,
            this.contextItemRefresh,
            this.toolStripMenuItem4,
            this.contextItemRename,
            this.contextItemDelete,
            this.toolStripMenuItem1,
            this.contextItemExplorer});
            this.contextMenuNode.Name = "node_MenuStrip";
            this.contextMenuNode.Size = new System.Drawing.Size(232, 166);
            // 
            // contextItemEdit2
            // 
            this.contextItemEdit2.Image = global::LiteNote.Properties.Resources.edit;
            this.contextItemEdit2.Name = "contextItemEdit2";
            this.contextItemEdit2.ShortcutKeyDisplayString = "F4";
            this.contextItemEdit2.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.contextItemEdit2.Size = new System.Drawing.Size(231, 30);
            this.contextItemEdit2.Text = "编辑";
            this.contextItemEdit2.Click += new System.EventHandler(this.ItemEdit_Click);
            // 
            // contextItemRefresh
            // 
            this.contextItemRefresh.Image = global::LiteNote.Properties.Resources.refresh;
            this.contextItemRefresh.Name = "contextItemRefresh";
            this.contextItemRefresh.ShortcutKeyDisplayString = "F5";
            this.contextItemRefresh.Size = new System.Drawing.Size(231, 30);
            this.contextItemRefresh.Text = "刷新";
            this.contextItemRefresh.Click += new System.EventHandler(this.ItemRefresh_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(228, 6);
            // 
            // contextItemRename
            // 
            this.contextItemRename.Image = global::LiteNote.Properties.Resources.rename;
            this.contextItemRename.Name = "contextItemRename";
            this.contextItemRename.Size = new System.Drawing.Size(231, 30);
            this.contextItemRename.Text = "重命名";
            this.contextItemRename.Click += new System.EventHandler(this.ItemRename_Click);
            // 
            // contextItemDelete
            // 
            this.contextItemDelete.Image = global::LiteNote.Properties.Resources.delete;
            this.contextItemDelete.Name = "contextItemDelete";
            this.contextItemDelete.Size = new System.Drawing.Size(231, 30);
            this.contextItemDelete.Text = "删除";
            this.contextItemDelete.Click += new System.EventHandler(this.ItemDelete_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(228, 6);
            // 
            // contextItemExplorer
            // 
            this.contextItemExplorer.Image = global::LiteNote.Properties.Resources.explorer;
            this.contextItemExplorer.Name = "contextItemExplorer";
            this.contextItemExplorer.Size = new System.Drawing.Size(231, 30);
            this.contextItemExplorer.Text = "在Explorer中打开";
            this.contextItemExplorer.Click += new System.EventHandler(this.ItemExplorer_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabelFold,
            this.toolStripStatusLabel1,
            this.statusLabelNode});
            this.statusStrip1.Location = new System.Drawing.Point(0, 737);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 18, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1227, 29);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // statusLabelFold
            // 
            this.statusLabelFold.Name = "statusLabelFold";
            this.statusLabelFold.Size = new System.Drawing.Size(30, 24);
            this.statusLabelFold.Text = "    ";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(1148, 24);
            this.toolStripStatusLabel1.Spring = true;
            // 
            // statusLabelNode
            // 
            this.statusLabelNode.Name = "statusLabelNode";
            this.statusLabelNode.Size = new System.Drawing.Size(30, 24);
            this.statusLabelNode.Text = "    ";
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.Filter = "*.txt";
            this.fileSystemWatcher1.SynchronizingObject = this;
            this.fileSystemWatcher1.Changed += new System.IO.FileSystemEventHandler(this.fileSystemWatcher1_Changed);
            // 
            // openFileDialogEditApp
            // 
            this.openFileDialogEditApp.Filter = "execute files|*.exe";
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // contextMenuWorkset
            // 
            this.contextMenuWorkset.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuWorkset.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contextItemWorksetAddNote,
            this.contextItemWorksetAddFold,
            this.toolStripMenuItem9,
            this.contextItemWorksetRename,
            this.contextItemWorksetRemove,
            this.toolStripMenuItem8,
            this.contextItemWorksetExplorer});
            this.contextMenuWorkset.Name = "contextMenuWorkset";
            this.contextMenuWorkset.Size = new System.Drawing.Size(232, 166);
            // 
            // contextItemWorksetAddNote
            // 
            this.contextItemWorksetAddNote.Image = global::LiteNote.Properties.Resources.note;
            this.contextItemWorksetAddNote.Name = "contextItemWorksetAddNote";
            this.contextItemWorksetAddNote.Size = new System.Drawing.Size(231, 30);
            this.contextItemWorksetAddNote.Text = "新建笔记";
            this.contextItemWorksetAddNote.Click += new System.EventHandler(this.contextItemWorksetAddNote_Click);
            // 
            // contextItemWorksetAddFold
            // 
            this.contextItemWorksetAddFold.Image = global::LiteNote.Properties.Resources.fold;
            this.contextItemWorksetAddFold.Name = "contextItemWorksetAddFold";
            this.contextItemWorksetAddFold.Size = new System.Drawing.Size(231, 30);
            this.contextItemWorksetAddFold.Text = "新建文件夹";
            this.contextItemWorksetAddFold.Click += new System.EventHandler(this.contextItemWorksetAddFold_Click);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(228, 6);
            // 
            // contextItemWorksetRename
            // 
            this.contextItemWorksetRename.Image = global::LiteNote.Properties.Resources.rename;
            this.contextItemWorksetRename.Name = "contextItemWorksetRename";
            this.contextItemWorksetRename.Size = new System.Drawing.Size(231, 30);
            this.contextItemWorksetRename.Text = "重命名";
            this.contextItemWorksetRename.Click += new System.EventHandler(this.ItemRename_Click);
            // 
            // contextItemWorksetRemove
            // 
            this.contextItemWorksetRemove.Image = global::LiteNote.Properties.Resources.delete;
            this.contextItemWorksetRemove.Name = "contextItemWorksetRemove";
            this.contextItemWorksetRemove.Size = new System.Drawing.Size(231, 30);
            this.contextItemWorksetRemove.Text = "移除";
            this.contextItemWorksetRemove.Click += new System.EventHandler(this.contextItemWorksetRemove_Click);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(228, 6);
            // 
            // contextItemWorksetExplorer
            // 
            this.contextItemWorksetExplorer.Image = global::LiteNote.Properties.Resources.explorer;
            this.contextItemWorksetExplorer.Name = "contextItemWorksetExplorer";
            this.contextItemWorksetExplorer.Size = new System.Drawing.Size(231, 30);
            this.contextItemWorksetExplorer.Text = "在Explorer中打开";
            this.contextItemWorksetExplorer.Click += new System.EventHandler(this.ItemExplorer_Click);
            // 
            // openFileDialogInsertImg
            // 
            this.openFileDialogInsertImg.Filter = "img files|*.png;*.bmp;*.jpg";
            // 
            // NoteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1227, 766);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuMain);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NoteForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LiteNote";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.NoteForm_Load);
            this.LocationChanged += new System.EventHandler(this.NoteForm_LocationChanged);
            this.Resize += new System.EventHandler(this.NoteForm_Resize);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.contextMenuBlack.ResumeLayout(false);
            this.contextMenuFold.ResumeLayout(false);
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            this.contextMenuNode.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.contextMenuWorkset.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView treeView1;
        //private MyTreeView treeViewNote;
        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.ToolStripMenuItem item文件;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem itemExit;
        private System.Windows.Forms.ToolStripMenuItem item帮助;
        private System.Windows.Forms.ToolStripMenuItem itemHelp;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem itemAbout;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ContextMenuStrip contextMenuFold;
        private System.Windows.Forms.ToolStripMenuItem itemLoad;
        private System.Windows.Forms.ToolStripMenuItem item设置;
        private System.Windows.Forms.ToolStripMenuItem contextItemAddNote;
        private System.Windows.Forms.ToolStripMenuItem contextItemAddFold;
        private System.Windows.Forms.ContextMenuStrip contextMenuNode;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem contextItemExplorer2;
        private System.Windows.Forms.ToolStripMenuItem contextItemEdit2;
        private System.Windows.Forms.ToolStripMenuItem contextItemRefresh;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem contextItemDelete;
        private System.Windows.Forms.ToolStripMenuItem contextItemRename;
        private System.Windows.Forms.ToolStripMenuItem item编辑;
        private System.Windows.Forms.ToolStripMenuItem itemRefresh;
        private System.Windows.Forms.ToolStripMenuItem itemEdit;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem itemAdd;
        private System.Windows.Forms.ToolStripMenuItem itemAddFold;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem itemRename;
        private System.Windows.Forms.ToolStripMenuItem itemDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem contextItemExplorer;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusLabelFold;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel statusLabelNode;
        private System.Windows.Forms.ToolStripMenuItem itemFontSize;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.TreeView treeView2;
        private System.Windows.Forms.ContextMenuStrip contextMenuBlack;
        private System.Windows.Forms.ToolStripMenuItem contextItemReload;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.ToolStripMenuItem itemEditApp;
        private System.Windows.Forms.OpenFileDialog openFileDialogEditApp;
        private System.Windows.Forms.ToolStripMenuItem itemIsCollapseBrother;
        private System.Windows.Forms.ToolStripMenuItem itemOneClickMode;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem7;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.ToolStripMenuItem itemAddWorkset;
        private System.Windows.Forms.ContextMenuStrip contextMenuWorkset;
        private System.Windows.Forms.ToolStripMenuItem contextItemWorksetRename;
        private System.Windows.Forms.ToolStripMenuItem contextItemWorksetRemove;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem contextItemWorksetAddFold;
        private System.Windows.Forms.ToolStripMenuItem contextItemWorksetAddNote;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem contextItemWorksetExplorer;
        private System.Windows.Forms.ToolStripMenuItem contextItemAddWorkset;
        private System.Windows.Forms.ToolStripMenuItem itemFind;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem itemCharset;
        private System.Windows.Forms.ToolStripMenuItem itemInsertImg;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem11;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem contextItemRename2;
        private System.Windows.Forms.ToolStripMenuItem contextItemDelete2;
        private System.Windows.Forms.OpenFileDialog openFileDialogInsertImg;
    }
}

