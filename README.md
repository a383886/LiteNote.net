c#来实现, QT所依赖的库太大不好移植

# LiteNoteApp 
自己写的轻量笔记项目，包含
* win32下的cs项目
* mac下用oc或swift
* android, ios原生
* ubuntu下用什么来实现比较好? qt? gtk?

# 灵感
* 如果想准备判断文件的编码的话，就得用iconv，但好像没有这个必要了。就直接默认utf8，然后让用户去指定特定文件的编码格式

# TODO
* 如果发现没有工程文件（或者监测到是第一次打开），则主动写个demo教程

## table的渲染
```
|          |   1    |   2  |   3  |    4   |
| -------- | -------- | -------- | -----| ------- |
| 1| 1.1 | 1.2  | 1.3  |  1.4 |
| 2| 2.1 |	2.2 | 2.3  |  2.4 |
| 3| 3.1 |	3.2 | 3.3  | 3.4 |

```

## 得将git折腾得够好一点

## 多线程查找


## 考虑在适当增加多点markdown格式
或者自定义支持哪些?

## 将帮助文档放在网页上

## 在ubuntu下找到一上合适的gui解决文案。
1. c# mono
2. gtk+
3. qt
4. python, Tkinter, wxpython, PyGTK
nautilus 是用gtk实现的

算了，先别在ubuntu下折腾了吧，最低的级别


# LiteNote.net
## LiteNodeSetting.settings 说明

LeftWidth 左边树控件的宽度 int
Left,Top,Width,Height 窗口的位置 int
FontSize 字体大小 int
EditApp 编译器的路径 string
RightWidth 右边树控件的宽度 int
IsCollapseBrother 是否收起兄弟节点 bool
OneClickMode	单击模式	bool
[dep]IsCollapseChild 是否收起子节点 bool
WorksetNames 工作集的名称 StringCollection
WorksetPaths 工作集的路径，和WorksetNames是平行数组 StringCollection



# dep
## 纯文本的换行
浏览器会自动将多个<br/> <p/>合并成一个，所以没必须研究多个空行了。

## QtLiteNote
用qt实现的可以跨平台使用的LiteNote，考虑将其开源。
一定得想把如何将Markdown实现好。
首先markdown是顺序结构，可以考虑用vector\<T\>将所有的内容存储

#LiteNoteCpp
用自己写的markdown渲染器进行md文件的显示

将md转化成html后，用XML解析器解析时，会将文本中的\n给去掉(这也是为什么写md文件中一个转行后，显示的html没有转行)，是不是得自己写个XML解析器呢?
123
abc

TreeView在很多节点时有点慢是DrawExpandNode()函数的问题，可以先判断节点如果超出了范围就不进行绘制，这样应该可以加快速度

##Bug
只要打开Linx/Vim文件夹，并且滚动下树控件，LiteNote的鼠标移动事件就不灵了
并且打开Linux/Vim后，将树控件往下拖，再点击打开LiteNice时树控件就没有内容显示了，这时因为offset没有设置好

            // dep: 更高级的方式是用个全局的通信机制，而不是用窗口名查找
            /*EnumWindows(myCallBack);

            if (hWnd != (IntPtr)0) {
                Utility.ShowWindow(hWnd, 1);
                Utility.SetForegroundWindow(hWnd);
                return;
            }*/
